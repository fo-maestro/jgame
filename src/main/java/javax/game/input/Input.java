package javax.game.input;

import javax.game.state.State;
import javax.game.util.Vector2;
import java.awt.event.*;

public final class Input implements KeyListener, MouseListener, MouseMotionListener {
    private static boolean[] keys = new boolean[256];
    private static boolean leftMouseButton;
    private static boolean rightMouseButton;
    private static int mouseX;
    private static int mouseY;

    private static Input inputManager = null;

    public static Input getInputManager() {
        if (inputManager == null) {
            return new Input();
        } else {
            return inputManager;
        }
    }

    public static boolean isKeyPressed(int key) {
        return keys[key];
    }

    public static boolean isLeftMouseButtonPressed() {
        return leftMouseButton;
    }

    public static boolean isRightMouseButtonPressed() {
        return rightMouseButton;
    }

    public static Vector2 getMousePosition() {
        return new Vector2(mouseX, mouseY);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseEvent.BUTTON1) {
            leftMouseButton = true;
        } else if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
            rightMouseButton = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseEvent.BUTTON1) {
            leftMouseButton = false;
        } else if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
            rightMouseButton = false;
        }

        State.getState().getUiManager().onMouseReleased();
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        mouseX = mouseEvent.getX();
        mouseY = mouseEvent.getY();

        State.getState().getUiManager().onMouseMove(mouseX, mouseY);
    }
}
