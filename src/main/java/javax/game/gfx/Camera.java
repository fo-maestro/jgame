/*
 * Camera.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.gfx;

import javax.game.display.Display;
import javax.game.entity.Entity;
import javax.game.tile.Tile;
import javax.game.util.Vector2;
import javax.game.world.World;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class Camera {
    private static Camera gameCamera = new Camera(0, 0);

    public static void setGameCamera(Camera camera) {
        gameCamera = camera;
    }

    public static Camera getGameCamera() {
        return gameCamera;
    }

    private Vector2 position;

    public Camera(float xOffset, float yOffset) {
        position = new Vector2(xOffset, yOffset);
    }

    public Camera(Vector2 position) {
        this(position.x, position.y);
    }

    private void checkBlankSpace() {
        if (position.x < 0) {
            position.x = 0;
        } else if (position.x > World.getWorld().getWidth() * Tile.getWidth() - Display.getViewPort().getWidth()) {
            position.x = World.getWorld().getWidth() * Tile.getWidth() - Display.getViewPort().getWidth();
        }

        if (position.y < 0) {
            position.y = 0;
        } else if (position.y > World.getWorld().getHeight() * Tile.getHeight() - Display.getViewPort().getHeight()) {
            position.y = World.getWorld().getHeight() * Tile.getHeight() - Display.getViewPort().getHeight();
        }
    }

    public void centerOnEntity(Entity e) {
        position.x = e.getPosition().x - Display.getViewPort().getWidth() / 2 + e.getWidth() / 2;
        position.y = e.getPosition().y - Display.getViewPort().getHeight() / 2 +  e.getHeight() / 2;
        checkBlankSpace();
    }

    public void move(float xMove, float yMove) {
        position.x += xMove;
        position.y += yMove;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(float xOffset, float yOffset) {
        position.x = xOffset;
        position.y = yOffset;
    }
}
