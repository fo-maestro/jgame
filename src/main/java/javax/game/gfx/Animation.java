package javax.game.gfx;

import javax.game.Time;
import java.awt.image.BufferedImage;

public class Animation {
    private int speed;
    private int index;
    private double timer;
    private BufferedImage[] frames;

    public Animation(int speed, BufferedImage... frames) {
        this.speed = speed;
        this.frames = frames;
        index = 0;
        timer = 0;
    }

    public void animate() {
        timer += Math.round(Time.deltaTime());

        if (timer >= speed) {
            index = index == frames.length - 1 ? 0 : index + 1;
            timer = 0;
        }
    }

    public BufferedImage getCurrentFrame() {
        return frames[index];
    }
}
