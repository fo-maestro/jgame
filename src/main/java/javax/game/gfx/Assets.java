package javax.game.gfx;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public final class Assets {
    private Assets() {}

    private static Map<String, Map<String, BufferedImage>> assets = new HashMap<>();

    private static String[] arrayCast(JSONArray array) {
        Object[] arrayT = array.toList().toArray();
        return Arrays.copyOf(arrayT, arrayT.length, String[].class);
    }

    public static void loadAssets(InputStream fileName) {
        JSONTokener tokener = new JSONTokener(fileName);
        JSONObject root = new JSONObject(tokener);

        root.getJSONArray("map").forEach(json -> {
            JSONObject element = (JSONObject) json;

            element.getJSONArray("assets").forEach(jsonAsset -> {
                JSONObject assets = (JSONObject) jsonAsset;

                JSONObject size = assets.getJSONObject("size");
                JSONObject offset = assets.getJSONObject("offset");

                loadAssets(root.getString("file"), size.getInt("width"), size.getInt("height"),
                        offset.getInt("top"), offset.getInt("left"), offset.getInt("bottom"), offset.getInt("right"),
                        element.getString("groupName"), arrayCast(assets.getJSONArray("names")));
            });
        });
    }

    public static void loadAssets(String spriteSheetPath, int spriteWidth, int spriteHeight, String groupName,
                                  String... assets) {
        loadAssets(spriteSheetPath, spriteWidth, spriteHeight, 0, 0, groupName, assets);
    }

    public static void loadAssets(String spriteSheetPath, int spriteWidth, int spriteHeight,
                                  int topOffset, int leftOffset,  String groupName, String... assets) {
        loadAssets(spriteSheetPath, spriteWidth, spriteHeight, topOffset, leftOffset, 0, 0, groupName, assets);
    }

    public static void loadAssets(String spriteSheetPath, int spriteWidth, int spriteHeight,
                                   int topOffset, int leftOffset, int bottomOffset, int rightOffset,
                                   String groupName, String... assets) {
        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage(spriteSheetPath));

        if (!Assets.assets.containsKey(groupName)) {
            Assets.assets.put(groupName, new HashMap<>());
        }

        Map<String, BufferedImage> group = Assets.assets.get(groupName);

        Iterator<String> it = Arrays.asList(assets).iterator();

        for (int y = spriteHeight * topOffset; y < sheet.getHeight() - spriteHeight * bottomOffset; y += spriteHeight) {
            for (int x = spriteWidth * leftOffset; x < sheet.getWidth() - spriteWidth * rightOffset; x += spriteWidth) {
                if (it.hasNext()) {
                    group.put(it.next(), sheet.getSprite(x, y, spriteWidth, spriteHeight));
                } else {
                    break;
                }
            }
        }
    }

    public static Map<String, BufferedImage> getGroup(String groupName) {
        return assets.get(groupName);
    }

    public static BufferedImage getAsset(String groupName, String assetName) {
        return assets.get(groupName).get(assetName);
    }
}
