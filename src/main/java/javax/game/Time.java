package javax.game;

public final class Time {
    static double deltaTime = 0;

    public static double deltaTime() {
        return deltaTime;
    }
}
