package javax.game.state;

import javax.game.Renderable;
import javax.game.entity.EntityManager;
import javax.game.ui.UIManager;
import javax.game.world.World;
import java.awt.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class State implements Renderable {
    private static State currentState = null;

    private boolean resourceLoaded = false;
    private EntityManager entityManager = new EntityManager();
    private UIManager uiManager = new UIManager();

    public void loadResources() {

    }

    public static void setState(State state) {
        if (!state.resourceLoaded) {
            state.loadResources();
            state.resourceLoaded = true;
        }

        currentState = state;
    }

    public static State getState() {
        return currentState;
    }

    @Override
    public void update() {
        entityManager.update();
        uiManager.update();
    }

    @Override
    public void render(Graphics g) {
        entityManager.render(g);
        uiManager.render(g);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public UIManager getUiManager() {
        return uiManager;
    }
}
