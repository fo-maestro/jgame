/*
 * ViewPort.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.display;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class ViewPort {
    private int width;
    private int height;

    public ViewPort(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
