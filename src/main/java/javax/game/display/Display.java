package javax.game.display;

import javax.swing.*;
import java.awt.*;

public class Display {
    private static Display currentDisplay;
    private static ViewPort currentViewport;

    public static Display getCurrentDisplay() {
        return currentDisplay;
    }

    public static void setViewPort(ViewPort viewPort) {
        currentViewport = viewPort;
        currentDisplay.getCanvas().setSize(viewPort.getWidth(), viewPort.getHeight());
        currentDisplay.getCanvas().revalidate();
    }

    public static ViewPort getViewPort() {
        return currentViewport;
    }

    private JFrame frame;
    private Canvas canvas;

    private Screen mode;

    public Display(String title, Screen mode) {
        createDisplay(title);
        this.mode = mode;

        frame.setLocationRelativeTo(null);
    }

    public Display(String title, int width, int height) {
        createDisplay(title);

        frame.setSize(width, height);
        frame.setLocationRelativeTo(null);
    }

    private void createDisplay(String title) {
        frame = new JFrame(title);
        frame.getContentPane().setBackground(Color.BLACK);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        frame.setResizable(false);

        canvas = new Canvas();
        canvas.setBackground(Color.WHITE);
        canvas.setFocusable(false);

        frame.add(canvas);
    }

    public void show() {
        currentDisplay = this;

        if (mode != null) {
            frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

            if (mode == Screen.FULL_SCREEN) {
                frame.setUndecorated(true);
            }
        }

        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public enum Screen { WINDOW_MODE, FULL_SCREEN }
}
