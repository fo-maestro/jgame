package javax.game.ui;

public interface MouseListener {
    void onMouseMove(int x, int y);
    void onMouseReleased();
}
