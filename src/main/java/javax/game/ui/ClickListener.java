package javax.game.ui;

public interface ClickListener {
    void onClick();
}
