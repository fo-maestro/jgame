package javax.game.ui;

import javax.game.Renderable;
import javax.game.input.Input;
import javax.game.util.Vector2;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class UIManager implements Renderable, MouseListener {
    private List<UIObject> uiObjects = new CopyOnWriteArrayList<>();

    public void addUIObject(UIObject uiObject) {
        uiObjects.add(uiObject);
    }

    public void removeUIObject(UIObject uiObject) {
        uiObjects.remove(uiObject);
    }

    @Override
    public void update() {
        uiObjects.forEach(UIObject::update);
    }

    @Override
    public void render(Graphics g) {
        uiObjects.forEach(uiObject -> uiObject.render(g));
    }

    @Override
    public void onMouseMove(int x, int y) {
        uiObjects.forEach(uiObject -> uiObject.onMouseMove(x, y));
    }

    @Override
    public void onMouseReleased() {
        uiObjects.forEach(UIObject::onMouseReleased);
    }
}
