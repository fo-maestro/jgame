package javax.game.ui;

import javax.game.Renderable;
import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class UIObject implements Renderable, MouseListener {
    private float x;
    private float y;
    private int width;
    private int height;
    private Rectangle bounds;
    private boolean hovering;
    private ClickListener listener;
    private BufferedImage[] buttonImage;

    public UIObject(float x, float y, int width, int height, BufferedImage[] buttonImage) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.buttonImage = buttonImage;
        hovering = false;
        bounds = new Rectangle((int) x, (int) y, width, height);
    }

    public void setOnClickListener(ClickListener listener) {
        this.listener = listener;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        bounds.setLocation((int) x, (int) y);
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        bounds.setLocation((int) x, (int) y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        bounds.setSize(width, height);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        bounds.setSize(width, height);
    }

    public boolean isHovering() {
        return hovering;
    }

    public void setButtonImage(BufferedImage[] buttonImage) {
        this.buttonImage = buttonImage;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(hovering ? buttonImage[0] : buttonImage[1], (int) x, (int) y, width, height, null);
    }

    @Override
    public void onMouseMove(int x, int y) {
        hovering = bounds.contains(x, y);
    }

    @Override
    public void onMouseReleased() {
        if (hovering && listener != null) {
            listener.onClick();
        }
    }
}
