/*
 * Vector2.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.util;

import java.awt.*;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class Vector2 {
    public static final int FORWARD = 1;
    public static final int BACKWARD = -1;

    public float x;
    public float y;

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void move(float amount, Direction direction) {
        switch (direction) {
            case LEFT:
                x += BACKWARD * amount;
                break;

            case RIGHT:
                x += FORWARD * amount;
                break;

            case UP:
                y += BACKWARD * amount;
                break;

            case DOWN:
                y += FORWARD * amount;
                break;
        }
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void normalize() {
        x = 0;
        y = 0;
    }

    public static float distance(float x1, float y1, float x2, float y2) {
        return (float) new Point((int) x1, (int) y1).distance((int) x2, (int) y2);
    }

    public static float distance(Vector2 vector1, Vector2 vector2) {
        return distance(vector1.x, vector1.y, vector2.x, vector2.y);
    }

    public float distance(float x, float y) {
        return distance(this.x, this.y, x, y);
    }

    public float distance(Vector2 other) {
        return distance(this, other);
    }

    public static Vector2 sum(float x1, float y1, float x2, float y2) {
        return new Vector2(x1 + x2, y1 + y2);
    }

    public static Vector2 sum(Vector2 vector1, Vector2 vector2) {
        return sum(vector1.x, vector1.y, vector2.x, vector2.y);
    }

    public Vector2 sum(float x, float y) {
        return sum(this.x, this.y, x, y);
    }

    public Vector2 sum(Vector2 other) {
        return sum(this, other);
    }

    public static Vector2 sub(float x1, float y1, float x2, float y2) {
        return new Vector2(x1 - x2, y1 - y2);
    }

    public static Vector2 sub(Vector2 vector1, Vector2 vector2) {
        return sub(vector1.x, vector1.y, vector2.x, vector2.y);
    }

    public Vector2 sub(float x, float y) {
        return sub(this.x, this.y, x, y);
    }

    public Vector2 sub(Vector2 other) {
        return sub(this, other);
    }

    public static Vector2 mult(float x, float y, float scalar) {
        return new Vector2(scalar * x, scalar * y);
    }

    public static Vector2 mult(Vector2 vector, float scalar) {
        return mult(vector.x, vector.y, scalar);
    }

    public Vector2 mult(float scalar) {
        return mult(this, scalar);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }
}
