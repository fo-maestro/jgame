/*
 * Utils.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class Utils {
    private Utils() {}

    public static String loadFileAsString(String path) {
        StringBuilder builder = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            for (String line = br.readLine(); line != null; line = br.readLine()) {
                builder.append(line).append("\n");
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    public static int parseInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            e.printStackTrace();

            return 0;
        }
    }
}
