package javax.game.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Consumer;

public final class Paint {
    private Paint() {
    }

    public static BufferedImage createGraphics(int width, int height, Consumer<Graphics> paint) {
        BufferedImage buff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        paint.accept(buff.createGraphics());

        return buff;
    }
}
