package javax.game;

import java.awt.*;

public interface Renderable {
    void update();
    void render(Graphics g);
}
