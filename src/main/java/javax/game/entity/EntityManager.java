package javax.game.entity;

import javax.game.Renderable;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class EntityManager implements Renderable {
    private Map<String, Entity> mapEntities = new HashMap<>();
    private List<Entity> entities = new CopyOnWriteArrayList<>();

    public void addEntity(Entity e) {
        if (mapEntities.put(e.getName(), e) == null) {
            entities.add(e);
        }
    }

    public void removeEntity(Entity e) {
        mapEntities.remove(e.getName());
        entities.remove(e);
    }

    @Override
    public void update() {
        entities.forEach(Entity::update);

        entities.sort((entity, t1) -> {
            if (entity.getPosition().y < t1.getPosition().y) {
                return  - 1;
            }

            return 1;
        });
    }

    @Override
    public void render(Graphics g) {
        entities.forEach(entity -> entity.render(g));
    }

    public Entity getEntity(String name) {
        return mapEntities.get(name);
    }

    public List<Entity> getEntities() {
        return entities;
    }
}
