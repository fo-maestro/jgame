package javax.game.entity;

import javax.game.Renderable;
import javax.game.gfx.Animation;
import javax.game.gfx.Camera;
import javax.game.state.State;
import javax.game.tile.Tile;
import javax.game.util.Vector2;
import javax.game.world.World;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

public abstract class Entity implements Renderable {
    protected Vector2 position;
    protected Vector2 movement;

    private int width;
    private int height;

    private String name;

    private Rectangle boundingBox;

    private BufferedImage defaultRender;
    private BufferedImage renderObject;
    private Animation animation;
    private static Camera entityCamera = new Camera(0, 0);

    public Entity(String name, Vector2 position, int width, int height, BufferedImage renderObject) {
        this(name, position.x, position.y, width, height, renderObject);
    }

    public Entity(String name, float x, float y, int width, int height, BufferedImage renderObject) {
        position = new Vector2(x, y);
        this.width = width;
        this.height = height;
        this.renderObject = renderObject;
        this.name = name;
        defaultRender = renderObject;

        movement = new Vector2(0, 0);
        boundingBox = new Rectangle(0, 0, width, height);
    }

    public abstract boolean isSolid();

    public String getName() {
        return name;
    }

    public Rectangle getCollisionBounds(float xOffset, float yOffset) {
        return new Rectangle((int) (position.x + boundingBox.x + xOffset), (int) (position.y + boundingBox.y + yOffset), boundingBox.width, boundingBox.height);
    }

    public boolean checkEntityCollisions(float xOffset, float yOffset) {
        List<Entity> entities = State.getState().getEntityManager().getEntities();

        for (int i = 0; i < entities.size(); i++) {
            if (entities.get(i).equals(this)) {
                continue;
            }

            if (entities.get(i).isSolid()) {
                if (entities.get(i).getCollisionBounds(0f, 0f).intersects(getCollisionBounds(xOffset, yOffset))) {
                    onCollision(entities.get(i));

                    return true;
                }
            }
        }

        return false;
    }

    public void setBoundingBox(Rectangle boundingBox) {
        this.boundingBox = boundingBox;
    }

    public void attachOnEntity(Camera camera) {
        entityCamera = camera;
    }

    public Camera getEntityCamera() {
        return entityCamera;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position.x = position.x;
        this.position.y = position.y;
    }

    public void setPosition(float x, float y) {
        position.setPosition(x, y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setAnimation(Animation animation) {
        if (animation == null) {
            renderObject = defaultRender;
        }

        this.animation = animation;
    }

    protected void fixUpdate() {
    }

    @Override
    public void update() {
        movement.normalize();
        fixUpdate();

        if (animation != null) {
            animation.animate();
            renderObject = animation.getCurrentFrame();
        }

        move();
    }

    private void move() {
        if (!checkEntityCollisions(movement.x, 0f)) {
            moveX();
        }

        if (!checkEntityCollisions(0f, movement.y)) {
            moveY();
        }
    }

    private void moveX() {
        if (movement.x > 0) {
            int tx = (int) (position.x + movement.x + boundingBox.x + boundingBox.width) / Tile.getWidth();

            Tile upperBoundTile = collisionWithTerrain(tx, (int) (position.y + boundingBox.y) / Tile.getHeight());
            Tile lowerBoundTile = collisionWithTerrain(tx, (int) (position.y + boundingBox.y + boundingBox.height) / Tile.getHeight());

            if (!upperBoundTile.isSolid() && !lowerBoundTile.isSolid()) {
                position.x += movement.x;
            } else {
                position.x = tx *  Tile.getWidth() - boundingBox.x - boundingBox.width - 1;
            }
        } else if (movement.x < 0) {
            int tx = (int) (position.x + movement.x + boundingBox.x) / Tile.getWidth();

            Tile upperBoundTile = collisionWithTerrain(tx, (int) (position.y + boundingBox.y) / Tile.getHeight());
            Tile lowerBoundTile = collisionWithTerrain(tx, (int) (position.y + boundingBox.y + boundingBox.height) / Tile.getHeight());

            if (!upperBoundTile.isSolid() && !lowerBoundTile.isSolid()) {
                position.x += movement.x;
            } else {
                position.x = tx * Tile.getWidth() + Tile.getWidth() - boundingBox.x;
            }
        }
    }

    private void moveY() {
        if (movement.y < 0) {
            int ty = (int) (position.y + movement.y + boundingBox.y) / Tile.getHeight();

            Tile rightBoundTile = collisionWithTerrain((int) (position.x + boundingBox.x) / Tile.getWidth(), ty);
            Tile leftBoundTile = collisionWithTerrain((int) (position.x + boundingBox.x + boundingBox.width) / Tile.getWidth(), ty);

            if (!rightBoundTile.isSolid() && !leftBoundTile.isSolid()) {
                position.y += movement.y;
            } else {
                position.y = ty * Tile.getHeight() + Tile.getHeight() - boundingBox.y;
            }
        } else if (movement.y > 0) {
            int ty = (int) (position.y + movement.y + boundingBox.y + boundingBox.height) / Tile.getHeight();

            Tile rightBoundTile = collisionWithTerrain((int) (position.x + boundingBox.x) / Tile.getWidth(), ty);
            Tile leftBoundTile = collisionWithTerrain((int) (position.x + boundingBox.x + boundingBox.width) / Tile.getWidth(), ty);

            if (!rightBoundTile.isSolid() && !leftBoundTile.isSolid()) {
                position.y += movement.y;
            } else {
                position.y = ty * Tile.getHeight() - boundingBox.y - boundingBox.height - 1;
            }
        }
    }

    private Tile collisionWithTerrain(int x, int y) {
        if (x < 0 || y < 0 || x >= World.getWorld().getWidth() || y >= World.getWorld().getHeight()) {
            return Tile.getTile(-1);
        }

        return Tile.getTile(World.getWorld().getTiles()[x][y]);
    }

    public void onDestroy() {
    }

    public void destroy() {
        onDestroy();
        State.getState().getEntityManager().removeEntity(this);
    }

    protected void onCollision(Entity object) {

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(renderObject, (int) (position.x - entityCamera.getPosition().x), (int) (position.y - entityCamera.getPosition().y), width, height, null);
    }
}
