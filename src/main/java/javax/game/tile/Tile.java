/*
 * Tile.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.tile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public abstract class Tile {
    private static Map<Integer, Tile> tileMap = new HashMap<>();

    private static int width;
    private static int height;
    private static Tile defaultTile;

    public static void setDefaultTile(Tile tile) {
        defaultTile = tile;
    }

    public static Tile getTile(int id) {
        Tile tile = tileMap.get(id);

        if (tile == null) {
            return defaultTile;
        }

        return tile;
    }

    public static void createTile(Tile tile) {
        tileMap.put(tile.getId(), tile);
    }

    public static void setSize(int width, int height) {
        Tile.width = width;
        Tile.height = height;
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static void removeTile(int id) {
        tileMap.remove(id);
    }

    private BufferedImage texture;
    private final int id;

    public Tile(BufferedImage texture, int id) {
        this.texture = texture;
        this.id = id;
    }

    public abstract boolean isSolid();

    public void render(Graphics g, int x, int y) {
        g.drawImage(texture, x, y, width, height, null);
    }

    public int getId() {
        return id;
    }
}
