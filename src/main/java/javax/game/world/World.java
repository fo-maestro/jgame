/*
 * World.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.world;

import javax.game.display.Display;
import javax.game.gfx.Camera;
import javax.game.tile.Tile;
import javax.game.util.Utils;
import java.awt.*;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class World {
    private static World currentWorld = new World();

    private int width;
    private int height;

    private int[][] tiles;

    public static void loadWorld(String path) {
        currentWorld = new World();

        String file = Utils.loadFileAsString(path);

        String[] tokens = file.split("\\s+");

        currentWorld.width = Utils.parseInt(tokens[0]);
        currentWorld.height = Utils.parseInt(tokens[1]);

        currentWorld.tiles = new int[currentWorld.width][currentWorld.height];

        for (int y = 0; y < currentWorld.height; y++) {
            for (int x = 0; x < currentWorld.width; x++) {
                currentWorld.tiles[x][y] = Utils.parseInt(tokens[(x +  y * currentWorld.width) + 2]);
            }
        }
    }

    public static World getWorld() {
        return currentWorld;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int[][] getTiles() {
        return tiles;
    }

    public void render(Graphics g) {
        Camera camera = Camera.getGameCamera();

        int xStart = (int) Math.max(0, camera.getPosition().x / Tile.getWidth());
        int xEnd = (int) Math.min(width, (camera.getPosition().x + Display.getViewPort().getWidth()) / Tile.getWidth() + 1);

        int yStart = (int) Math.max(0, camera.getPosition().y / Tile.getHeight());
        int yEnd = (int) Math.min(height, (camera.getPosition().y + Display.getViewPort().getHeight()) / Tile.getHeight() + 1);

        for (int y = yStart; y < yEnd; y++) {
            for (int x = xStart; x < xEnd; x++) {
                Tile.getTile(tiles[x][y]).render(g, (int) (x * Tile.getWidth() - camera.getPosition().x),
                        (int) (y *  Tile.getHeight() - camera.getPosition().y));

            }
        }
    }
}
