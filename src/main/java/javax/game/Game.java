package javax.game;

import javax.game.display.Display;
import javax.game.input.Input;
import javax.game.state.State;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.concurrent.TimeUnit;

public abstract class Game implements Runnable {
    private Display display;

    private boolean running;

    private Thread runner;

    private int fps;
    private double frameTime;

    public Game(int fps, String title, int width, int height) {
        this.fps = fps;
        frameTime = TimeUnit.SECONDS.toNanos(1) / fps;

        display = new Display(title, width, height);
        display.getFrame().addKeyListener(Input.getInputManager());
        display.getFrame().addMouseListener(Input.getInputManager());
        display.getFrame().addMouseMotionListener(Input.getInputManager());
        display.getCanvas().addMouseListener(Input.getInputManager());
        display.getCanvas().addMouseMotionListener(Input.getInputManager());
    }

    public Game(int fps, String title, Display.Screen mode) {
        this.fps = fps;
        frameTime = TimeUnit.SECONDS.toNanos(1) / fps;

        display = new Display(title, mode);
        display.getFrame().addKeyListener(Input.getInputManager());
    }

    protected abstract void init();

    @Override
    public void run() {
        display.show();
        init();

        long now;
        long lastTime = System.nanoTime();

        while (running) {
            now = System.nanoTime();
            Time.deltaTime += (now - lastTime) / frameTime;
            lastTime = now;

            if (Time.deltaTime >= 1) {
                update();
                render();
                Time.deltaTime--;
            }
        }

        stop();
    }

    private void update() {
        if (State.getState() != null) {
            State.getState().update();
        }
    }

    private void render() {
        BufferStrategy bs = display.getCanvas().getBufferStrategy();

        if (bs == null) {
            display.getCanvas().createBufferStrategy(3);

            return;
        }

        Graphics g = bs.getDrawGraphics();

        g.clearRect(0, 0, display.getCanvas().getWidth(), display.getCanvas().getHeight());

        if (State.getState() != null) {
            State.getState().render(g);
        }

        bs.show();
        g.dispose();
    }

    public synchronized void start() {
        if (running) {
            return;
        }

        running = true;
        runner = new Thread(this);
        runner.start();
    }

    public synchronized void stop() {
        if (!running) {
            return;
        }

        running = false;

        try {
            runner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Display getDisplay() {
        return display;
    }

    public int getFps() {
        return fps;
    }

    public void setFps(int fps) {
        this.fps = fps;
        frameTime = TimeUnit.SECONDS.toNanos(1) / fps;
    }
}
