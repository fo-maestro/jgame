package javax.game.state;

import javax.game.display.Display;
import javax.game.display.ViewPort;
import javax.game.entity.creatures.Player;
import javax.game.entity.creatures.Tree;
import javax.game.gfx.Camera;
import javax.game.tile.Tile;
import javax.game.util.Vector2;
import javax.game.world.World;
import java.awt.*;

public class GameState extends State {

    public GameState() {
        Camera camera = new Camera(0, 0);
        Player player = new Player("Player 1",100, 100);

        player.attachOnEntity(camera);
        Display.setViewPort(new ViewPort(800, 570));
        Camera.setGameCamera(camera);

        getEntityManager().addEntity(player);
        getEntityManager().addEntity(new Tree(new Vector2(150, 250), 32, 64));
    }


    @Override
    public void loadResources() {
        World.loadWorld("worlds/world1.txt");
    }

    @Override
    public void render(Graphics g) {
        World.getWorld().render(g);
        super.render(g);
    }
}
