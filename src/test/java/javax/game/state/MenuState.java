package javax.game.state;

import javax.game.display.Display;
import javax.game.display.ViewPort;
import javax.game.input.Input;
import javax.game.ui.Button;
import javax.game.util.Vector2;
import java.awt.*;

public class MenuState extends State {

    public MenuState() {
        Display.setViewPort(new ViewPort(800, 570));
        getUiManager().addUIObject(new Button(200, 200, 128, 64));
    }
}
