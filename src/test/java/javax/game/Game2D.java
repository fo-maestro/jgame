package javax.game;

import javax.game.display.Display;
import javax.game.gfx.Assets;
import javax.game.state.GameState;
import javax.game.state.MenuState;
import javax.game.state.State;
import javax.game.tile.DirtTile;
import javax.game.tile.GrassTile;
import javax.game.tile.RockTile;
import javax.game.tile.Tile;

public class Game2D extends Game {

    public Game2D(String title, int width, int height) {
        super(60, title, width, height);
    }

    public Game2D(String title, Display.Screen mode) {
        super(60, title, mode);
    }

    @Override
    protected void init() {
        Assets.loadAssets(getClass().getResourceAsStream("/init/assets.json"));

        Tile.createTile(new GrassTile(0));
        Tile.createTile(new DirtTile(1));
        Tile.createTile(new RockTile(2));
        Tile.setSize(64, 64);
        Tile.setDefaultTile(Tile.getTile(0));

        State.setState(new MenuState());
    }
}
