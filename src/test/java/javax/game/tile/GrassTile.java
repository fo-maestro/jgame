/*
 * GrassTile.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.tile;

import javax.game.gfx.Assets;
import java.awt.*;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class GrassTile extends Tile {

    public GrassTile(int id) {
        super(Assets.getAsset("terrain", "grass"), id);
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
