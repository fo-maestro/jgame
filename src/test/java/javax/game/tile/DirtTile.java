/*
 * DirtTile.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package javax.game.tile;

import javax.game.gfx.Assets;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class DirtTile extends Tile {

    public DirtTile(int id) {
        super(Assets.getAsset("terrain", "dirt"), id);
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
