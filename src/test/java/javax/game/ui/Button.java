package javax.game.ui;

import javax.game.gfx.Assets;
import javax.game.state.GameState;
import javax.game.state.State;
import java.awt.image.BufferedImage;

public class Button extends UIObject {


    public Button(float x, float y, int width, int height) {
        super(x, y, width, height, new BufferedImage[] {Assets.getAsset("ui", "button"),
                Assets.getAsset("ui", "button_hover")});

        setOnClickListener(() -> State.setState(new GameState()));
    }
}
