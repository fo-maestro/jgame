package javax.game.entity.creatures;

import javax.game.entity.Entity;
import javax.game.gfx.Animation;
import javax.game.gfx.Assets;
import javax.game.input.Input;
import javax.game.state.State;
import javax.game.util.Vector2;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Player extends Entity {

    private float speed = 3.0f;
    private Animation upAnim;
    private Animation downAnim;
    private Animation leftAnim;
    private Animation rightAnim;

    public Player(String name, float x, float y) {
        super("player", x, y, 64, 64, Assets.getAsset("animation_player1", "down_1"));
        setBoundingBox(new Rectangle(22, 30, 19, 33));
        upAnim = new Animation(25, Assets.getAsset("animation_player1", "up_1"),
                Assets.getAsset("animation_player1", "up_2"));

        downAnim = new Animation(25, Assets.getAsset("animation_player1", "down_1"),
                Assets.getAsset("animation_player1", "down_2"));

        leftAnim = new Animation(25, Assets.getAsset("animation_player1", "left_1"),
                Assets.getAsset("animation_player1", "left_2"));

        rightAnim = new Animation(25, Assets.getAsset("animation_player1", "right_1"),
                Assets.getAsset("animation_player1", "right_2"));

        setAnimation(downAnim);
    }

    @Override
    public boolean isSolid() {
        return true;
    }

    @Override
    protected void fixUpdate() {
        if (Input.isKeyPressed(KeyEvent.VK_W)) {
            movement.move(speed, Vector2.Direction.UP);
            setAnimation(upAnim);
        }

        if (Input.isKeyPressed(KeyEvent.VK_S)) {
            movement.move(speed, Vector2.Direction.DOWN);
            setAnimation(downAnim);
        }

        if (Input.isKeyPressed(KeyEvent.VK_A)) {
            movement.move(speed, Vector2.Direction.LEFT);
            setAnimation(leftAnim);
        }

        if (Input.isKeyPressed(KeyEvent.VK_D)) {
            movement.move(speed, Vector2.Direction.RIGHT);
            setAnimation(rightAnim);
        }

        getEntityCamera().centerOnEntity(this);
    }

    @Override
    protected void onCollision(Entity object) {
        object.destroy();
    }
}
