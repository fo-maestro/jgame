package javax.game.entity.creatures;

import javax.game.entity.Entity;
import javax.game.gfx.Assets;
import javax.game.util.Vector2;
import java.awt.*;

public class Tree extends Entity {
    public Tree(Vector2 position, int width, int height) {
        super("Tree_1", position, width, height, Assets.getAsset("static_entity", "tree"));
        setBoundingBox(new Rectangle(10, (int) (height / 1.5f), width - 20, (int) (height - height / 1.5f)));
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
